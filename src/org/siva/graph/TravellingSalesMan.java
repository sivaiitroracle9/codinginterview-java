package org.siva.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TravellingSalesMan {

	Coordinate start = new Coordinate(0, 0);
	Coordinate end = new Coordinate(4, 4);
	
	List<Integer> val = new ArrayList<Integer>();

	int end1 = 11;

	static Map<String, Integer> distanceWeightMap = new HashMap<String, Integer>();

	private String edgeKey(Integer i1, Integer i2) {
		if (i1 > i2)
			return String.valueOf(i1) + '-' + String.valueOf(i2);
		return String.valueOf(i2) + '-' + String.valueOf(i1);
	}

	private int minDist(int size, int distance, Integer cur,
			List<Integer> vertices, List<Integer> visited) {

		if (size == 0) {
			
			val.add(2);
			val.add(1);
			val.add(3);
			val.add(0);
			
			// return distanceWeightMap.get(edgeKey(cur, end1));
			System.out.println("visited-path:" + visited);
			return 10;
		}
		
		int dist = distance;
		for (Integer co : vertices) {
			if (!visited.contains(co)) {
				visited.add(co);
				String key = edgeKey(cur, co);
				System.out.println("Key=" + key);
				int cij = distanceWeightMap.get(key);
				int d = minDist(size - 1, distance, co, vertices, visited);
				visited.remove(co);

				dist = Math.min(cij + d, dist);
			}
		}
		return dist;

	}

	public static void main(String[] args) {
		TravellingSalesMan tsp = new TravellingSalesMan();

		int[][] mat = new int[4][4];
		mat[0][0] = 0;
		mat[0][1] = 15;
		mat[0][2] = 21;
		mat[0][3] = 12;
		mat[1][0] = 15;
		mat[1][1] = 0;
		mat[1][2] = 10;
		mat[1][3] = 10;
		mat[2][0] = 21;
		mat[2][1] = 10;
		mat[2][2] = 0;
		mat[2][3] = 11;
		mat[3][0] = 12;
		mat[3][1] = 10;
		mat[3][2] = 11;
		mat[3][3] = 0;

		List<Integer> vertices = new ArrayList<Integer>();
		vertices.add(0);
		vertices.add(1);
		vertices.add(2);
		vertices.add(3);
		
		List<Integer> visited = new ArrayList<Integer>();

		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				distanceWeightMap.put(tsp.edgeKey(i, j), mat[i][j]);
			}
		}

		//distanceWeightMap.put(tsp.edgeKey(, j), mat[i][j]);
		
		int start1 = 10;

		int distance = Integer.MAX_VALUE;
		for (Integer co : vertices) {
			if (!visited.contains(co)) {
				//int cij = distanceWeightMap.get(tsp.edgeKey(start1, co));
				visited.add(co);
				int cij = 10;
				int d = tsp.minDist(vertices.size() - 1, distance, co,
						vertices, visited);
				visited.remove(co);

				distance = Math.min(cij + d, distance);
			}
		}
		System.out.println(distance);
	}

}
class Coordinate {
	private int x;
	private int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int X() {
		return this.x;
	}

	public int Y() {
		return this.y;
	}

	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

	@Override
	public boolean equals(Object obj) {
		boolean equal = true;

		if (obj != null && obj instanceof Coordinate) {
			equal = equal && this.x == ((Coordinate) obj).x;
			equal = equal && this.y == ((Coordinate) obj).y;
			return equal;
		}
		return false;
	}
}