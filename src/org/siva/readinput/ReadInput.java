package org.siva.readinput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadInput {

	public static void main(String[] args) {

		ReadInput ri = new ReadInput();

		List<String> lines = new ArrayList<String>();
		List<Coordinate> checkpoints = new ArrayList<Coordinate>();
		List<Coordinate> closedblocks = new ArrayList<Coordinate>();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try {
			String text = br.readLine();
			int width = 0, height = 0, count = 0;
			String[] dimensions = text.split(" ");
			if (dimensions.length == 2) {
				width = Integer.parseInt(dimensions[0].trim());
				height = Integer.parseInt(dimensions[1].trim());
			} else {
				System.out.println("IO error trying to read your name!");
				System.exit(1);
			}

			while (text != null && !text.equals("")) {
				count++;
				text = br.readLine();
				String line = text.trim();
				char[] charArr = line.toCharArray();
				for (int i = 0; i < width; i++) {
					if (charArr[i] == '@') {
						Coordinate co = new Coordinate(i, count-1);
						checkpoints.add(co);
					}
				}

				lines.add(text);
				if (count == height)
					break;
			}

		} catch (IOException ioe) {
			System.out.println("IO error trying to read your name!");
			System.exit(1);
		}

		for (String line : lines) {
			System.out.println("Output:" + line);
		}

		String ou = "Output:";
		for (Coordinate co : checkpoints) {
			ou += co.toString();
		}
		System.out.println(ou);
	}

}

class Coordinate {
	private int x;
	private int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "-(" + this.x + "," + this.y + ")-";
	}
}
