package jp.co.worksap.global;

public class SegmentTree {

	public SegmentTreeNode constructTree(long[] array) {

		SegmentTreeNode root = null;

		root = insert(root, 0, array.length - 1, array);

		return root;
	}

	private SegmentTreeNode insert(SegmentTreeNode root, long rangeMin,
			long rangeMax, long[] array) {

		if (root == null) {
			root = new SegmentTreeNode();
			root.position = (rangeMin + rangeMax) / 2;
		}

		root.rangeMin = rangeMin;
		root.rangeMax = rangeMax;

		if (rangeMax == rangeMin) {
			root.data = array[(int) rangeMin];
			return root;
		}

		long rangeMid = (root.rangeMin + root.rangeMax) / 2;
		root.left = insert(root.left, rangeMin, rangeMid, array);
		root.left.level = root.level + 1;

		root.right = insert(root.right, rangeMid, rangeMax, array);
		root.right.level = root.level + 1;

		root.data = Math.max(root.left.data, root.right.data);
		return root;
	}

	/**
	 * 
	 * @param rangeMin
	 *            1 to n (length of array) & rangeMin <= rangeMax
	 * @param rangeMax
	 *            1 to n (length of array) & rangeMax >= rangeMin
	 * @return
	 */
	public long searchTree(SegmentTreeNode root, long queryRangeMin,
			long queryRangeMax) {

		if (root.rangeMin == queryRangeMin && root.rangeMax == queryRangeMax) {
			return root.data;
		}

		long rootRangeMid = (root.rangeMin + root.rangeMax) / 2;

		if (queryRangeMax >= rootRangeMid && queryRangeMin <= rootRangeMid) {
			long leftMax = searchTree(root.left, queryRangeMin, rootRangeMid);
			long rightMax = searchTree(root.right, rootRangeMid, queryRangeMax);
			return Math.max(leftMax, rightMax);
		} else if (queryRangeMin >= rootRangeMid) {
			long rightMax = searchTree(root.right, queryRangeMin, queryRangeMax);
			return rightMax;
		} else if (queryRangeMax <= rootRangeMid) {
			long leftMax = searchTree(root.left, queryRangeMin, queryRangeMax);
			return leftMax;
		}

		return 0L;
	}

	private long queryRangeMax(SegmentTreeNode root, long queryRangeMin,
			long queryRangeMax, long[] array) {
		if (!validateInput(queryRangeMin, queryRangeMax, array)) {
			return -1L;
		}

		return searchTree(root, queryRangeMin - 1, queryRangeMax - 1);
	}

	private boolean validateInput(long rangeMin, long rangeMax, long[] array) {

		if (rangeMin > rangeMax) {
			return false;
		}

		if (rangeMin < 1 || rangeMax > array.length) {
			return false;
		}

		return true;

	}

	class SegmentTreeNode {
		long rangeMin = 0L;
		long rangeMax = 0L;
		long data;
		SegmentTreeNode left = null;
		SegmentTreeNode right = null;

		// not required
		long level = 0L;
		long position = 0L;

		@Override
		public String toString() {
			return "(" + rangeMin + "-" + rangeMax + ", " + data + ")";
		}
	}
}
