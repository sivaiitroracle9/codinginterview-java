/**
 * 
 */
package jp.co.worksap.global;

import java.util.ArrayList;
import java.util.NoSuchElementException;
/**
 * @author Siva Kumar Edupuganti
 *
 */
/**
* The Queue class represents an immutable first-in-first-out (FIFO) queue of objects.
* @param <E>
*/
public class ImmutableQueue<E> {
	
	Node<E> first = null;
	Node<E> last = null;
	
	ArrayList<Node<E>> nodes;
	
	int size = 0;
		
/**
* requires default constructor.
*/
public ImmutableQueue() {
// modify this constructor if necessary, but do not remove default constructor
	this.nodes = new ArrayList<Node<E>>();
}

/**
* Returns the queue that adds an item into the tail of this queue without modifying this queue.
* <pre>
* e.g.
* When this queue represents the queue (2, 1, 2, 2, 6) and we enqueue the value 4 into this queue,
* this method returns a new queue (2, 1, 2, 2, 6, 4)
* and this object still represents the queue (2, 1, 2, 2, 6) .
* </pre>
* If the element e is null, throws IllegalArgumentException.
* @param e
* @return
* @throws IllegalArgumentException
*/

	public ImmutableQueue<E> enqueue(E e) {

		if (e == null) {
			throw new IllegalArgumentException();
		}

		Node<E> node = new Node<E>();
		node.data = e;

		ImmutableQueue<E> newQueue = new ImmutableQueue<E>();
		if (this.first == null && this.last == null) {
			newQueue.first = node;
			newQueue.last = newQueue.first;
		} else {
			newQueue.first = this.first;
			newQueue.last = node;
			this.last.next = newQueue.last;
			newQueue.nodes.addAll(this.nodes);
		}
		
		newQueue.nodes.add(node);
		newQueue.size = this.size + 1;
		return newQueue;
	}
	
	
/**
* Returns the queue that removes the object at the head of this queue without modifying this queue.
* <pre>
* e.g.
* When this queue represents the queue (7, 1, 3, 3, 5, 1) ,
* this method returns a new queue (1, 3, 3, 5, 1)
* and this object still represents the queue (7, 1, 3, 3, 5, 1) .
* </pre>
* If this queue is empty, throws java.util.NoSuchElementException.
* @return
* @throws java.util.NoSuchElementException
*/
public ImmutableQueue<E> dequeue() {
	
	if(this.size == 0){
		throw new NoSuchElementException();
	}
	
	ImmutableQueue<E> newQueue = new ImmutableQueue<E>();
	if (this.size==1) {
		newQueue.first = null;
		newQueue.last = null;
	} else {
		//Node<E> node = this.first;
		newQueue.first = this.first.next;
		if (this.last != this.first) {
			newQueue.last = this.last;
		} else {
			newQueue.last = newQueue.first;
		}
		
		newQueue.size = this.size -1;
	}
	
return newQueue;
}

/**
* Looks at the object which is the head of this queue without removing it from the queue.
* <pre>
* e.g.
* When this queue represents the queue (7, 1, 3, 3, 5, 1),
* this method returns 7 and this object still represents the queue (7, 1, 3, 3, 5, 1)
* </pre>
* If the queue is empty, throws java.util.NoSuchElementException.
* @return
* @throws java.util.NoSuchElementException
*/

	public E peek() {

		if (this.size == 0) {
			throw new NoSuchElementException();
		}

		Node<E> node = this.first;
		return node.data;
	}

/**
* Returns the number of objects in this queue.
* @return
*/
	public int size() {
		return this.size;
	}
}

class Node<E> {
	Node<E> next = null;
	E data;
}
