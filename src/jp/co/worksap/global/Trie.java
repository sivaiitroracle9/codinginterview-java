package jp.co.worksap.global;

import java.util.ArrayList;

/**
 * 
 * Trie: Basic Insert & Search
 * It is can be used for Dictionary, prefix finding in a string.
 * 
 * @author siva
 *
 */

public class Trie {
    
    private TrieNode constructTrie(String text) {
        
        TrieNode root = new TrieNode();
        
        String[] words = text.split(" ");
        
        for (String word : words) {
            boolean chidFound = false;
            TrieNode child = null;
            
            char[] wordArray = word.toCharArray();
            
            for (TrieNode node : root.getChildren()) {
                if (node.getCh() == wordArray[0]) {
                    child = node;
                    chidFound = true;
                }
            }
            
            if (!chidFound) {
                
                child = new TrieNode();
                root.getChildren().add(child);
                child.setCh(wordArray[0]);
                
            }
            
            insert(child, wordArray, 1);
        }
        
        return root;
    }
    
    private void insert(TrieNode node, char[] wordArray, int index) {
        
        if (wordArray.length == index) {
            return;
        }
        
        boolean chidFound = false;
        TrieNode child = null;
        
        for (TrieNode n : node.getChildren()) {
            if (n.getCh() == wordArray[index]) {
                child = node;
                chidFound = true;
            }
        }
        
        if (!chidFound) {
            
            child = new TrieNode();
            node.getChildren().add(child);
            child.setCh(wordArray[index]);
            
        }
        
        insert(child, wordArray, index + 1);
    }
    
}

class TrieNode {
    
    private char ch;
    private ArrayList<TrieNode> children = new ArrayList<TrieNode>();
    
    public char getCh() {
        return ch;
    }
    
    public void setCh(char ch) {
        this.ch = ch;
    }
    
    public ArrayList<TrieNode> getChildren() {
        return children;
    }
    
    public void setChildren(ArrayList<TrieNode> children) {
        this.children = children;
    }
    
}
