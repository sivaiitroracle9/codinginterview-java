package jp.co.worksap.global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Orienteering {

	private Point START;
	private Point GOAL;
	private int INFINITE = Integer.MAX_VALUE;

	private int xmin, xmax, ymin, ymax;

	private List<Coordinate> checkpoints = new ArrayList<Coordinate>();
	private List<Coordinate> closedblocks = new ArrayList<Coordinate>();

	// This map is to store the edges and their weights formed using the
	// START, GOAL and the Checkpoints.
	private Map<String, Integer> weightGraphMap = new HashMap<String, Integer>();

	// This map is to store the computed results of the minimum distance
	// from a checkpoint co-ordinate to the goal point along the specific
	// chekpoints.
	// Dynamic progrmming is used in this case to compute the minimum distance.
	private Map<String, Integer> minDistanceMap = new HashMap<String, Integer>();

	public static void main(String[] args) {

		Point START = null;
		Point GOAL = null;
		List<Coordinate> openblocks = new ArrayList<Coordinate>();
		List<Coordinate> checkpoints = new ArrayList<Coordinate>();
		List<Coordinate> closedblocks = new ArrayList<Coordinate>();
		int width = 0, height = 0;

		// Read Input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String text = br.readLine();
			int count = 0;
			String[] dimensions = text.split(" ");
			if (dimensions.length == 2) {
				width = Integer.parseInt(dimensions[0].trim());
				height = Integer.parseInt(dimensions[1].trim());
			} else {
				// System.out.println("Invalid Input!!");
				System.exit(1);
			}

			while (text != null && !text.equals("")) {
				count++;
				text = br.readLine();
				String line = text.trim();
				char[] charArr = line.toCharArray();
				for (int i = 0; i < width; i++) {
					Coordinate co = new Coordinate(i, count - 1);
					if (charArr[i] == '@') {
						checkpoints.add(co);
					} else if (charArr[i] == '.') {
						openblocks.add(co);
					} else if (charArr[i] == '#') {
						closedblocks.add(co);
					} else if (charArr[i] == 'S') {
						START = new Point(i, count - 1);
					} else if (charArr[i] == 'G') {
						GOAL = new Point(i, count - 1);
					}
				}
				if (count == height)
					break;
			}

		} catch (IOException ioe) {
			// System.out.println("IO error during Input");
			System.exit(1);
		}

		// Validate Input
		if (!validateInput(width, height, checkpoints.size())) {
			System.out.println(-1);
			System.exit(1);
		}

		// Game Creation and Start
		Orienteering game = new Orienteering(START, GOAL);
		game.setDimensions(width, height);
		game.setCheckPoints(checkpoints);
		game.setClosedBlocks(closedblocks);

		int dist = game.shortestDistance();

		if (dist == Integer.MAX_VALUE) {
			System.out.println(-1);
			System.exit(1);
		}
		System.out.println(dist);
	}

	public Orienteering(Point startPoint, Point goalPoint) {
		this.START = startPoint;
		this.GOAL = goalPoint;
	}

	public void setDimensions(int width, int height) {
		this.xmin = 0;
		this.ymin = 0;
		this.xmax = width - 1;
		this.ymax = height - 1;
	}

	public void setCheckPoints(List<Coordinate> checkpoints) {
		(this.checkpoints).addAll(checkpoints);
	}

	public void setClosedBlocks(List<Coordinate> closedblocks) {
		(this.closedblocks).addAll(closedblocks);
	}

	/**
	 * Compute the Shortest distance, based on the Travelling Sales Man
	 * Algorithm.
	 * 
	 * @return
	 */
	private int shortestDistance() {

		int dist = Integer.MAX_VALUE;
		if (checkpoints.size() > 0) {

			formWeightedGraph();

			List<Coordinate> visited = new ArrayList<Coordinate>();

			Coordinate start_c = new Coordinate(START.X(), START.Y());
			for (Coordinate co : checkpoints) {
				if (!visited.contains(co)) {
					visited.add(co);
					String key = edgeKey(start_c, co);
					// System.out.println("Key=" + key);
					int weight = weightGraphMap.get(key);
					int d = minDistance(checkpoints.size() - 1, dist, co,
							visited);
					visited.remove(co);

					dist = Math.min(weight + d, dist);
				}
			}
		} else {
			dist = shortestPathBFS(START, GOAL);
		}

		return dist;
	}

	private int minDistance(int size, int distance, Coordinate cur,
			List<Coordinate> visited) {

		if (size == 0) {
			Coordinate goal_c = new Coordinate(GOAL.X(), GOAL.Y());
			return weightGraphMap.get(edgeKey(cur, goal_c));
		}

		List<Coordinate> unvisited = new ArrayList<Coordinate>();
		unvisited.addAll(checkpoints);
		unvisited.removeAll(visited);

		String pathkey = pathKey(cur, unvisited);
		if (minDistanceMap.get(pathkey) != null) {
			// System.out.println("path-taken-from-cache-" + pathkey);
			return minDistanceMap.get(pathkey);
		}

		int dist = distance;
		for (Coordinate co : checkpoints) {
			if (!visited.contains(co)) {
				visited.add(co);
				String key = edgeKey(cur, co);
				int weight = weightGraphMap.get(key);
				int d = minDistance(size - 1, distance, co, visited);
				visited.remove(co);

				dist = Math.min(weight + d, dist);
			}
		}

		minDistanceMap.put(pathkey, dist);
		// System.out.println(pathkey + " # " + dist);
		return dist;
	}

	/**
	 * Form the weighted Graph with START, GOAL and the Checkpoints.
	 */
	private void formWeightedGraph() {

		// Forming Weighted Graph to Pass through All The Checkpoints
		// and the START and the GOAL once.
		int sdist = Integer.MAX_VALUE;
		// Find the weights from START to Checkpoints
		Coordinate start_c = new Coordinate(START.X(), START.Y());
		for (Coordinate co : checkpoints) {
			Point pco = new Point(co.X(), co.Y());
			sdist = shortestPathBFS(START, pco);

			weightGraphMap.put(edgeKey(start_c, co), sdist);
		}

		// Find the weights between the Checkpoints
		for (Coordinate co1 : checkpoints) {
			for (Coordinate co2 : checkpoints) {
				if (!weightGraphMap.containsKey(edgeKey(co1, co2))) {
					Point pco1 = new Point(co1.X(), co1.Y());
					Point pco2 = new Point(co2.X(), co2.Y());

					sdist = shortestPathBFS(pco1, pco2);
					weightGraphMap.put(edgeKey(co1, co2), sdist);
				}
			}
		}

		// Find the weights from GOAL to Checkpoints
		Coordinate goal_c = new Coordinate(GOAL.X(), GOAL.Y());
		for (Coordinate co : checkpoints) {
			Point pco = new Point(co.X(), co.Y());
			sdist = shortestPathBFS(GOAL, pco);

			weightGraphMap.put(edgeKey(goal_c, co), sdist);
		}

	}

	/**
	 * Directly compute the Shortest Path in a Graph using Breadth First Search
	 * Algorithm.
	 * 
	 * @param start
	 * @param goal
	 * @return
	 */
	private int shortestPathBFS(Point start, Point goal) {

		Coordinate goal_c = new Coordinate(goal.X(), goal.Y());

		int level = 0;
		start.setLevel(level);
		start.setVisited();

		LinkedList<Point> bfsQueue = new LinkedList<Point>();
		List<Coordinate> visitedList = new ArrayList<Coordinate>();
		bfsQueue.add(start);
		Coordinate startPoint_c = new Coordinate(start.X(), start.Y());
		visitedList.add(startPoint_c);

		while (!bfsQueue.isEmpty()) {
			Point point = bfsQueue.poll();

			Coordinate point_c = new Coordinate(point.X(), point.Y());
			if (goal_c.equals(point_c)) {
				return point.Level();
			}

			// Right
			Point right = point.right();
			Coordinate right_c = new Coordinate(right.X(), right.Y());
			if (this.isPointValid(right) && !visitedList.contains(right_c)
					&& !this.closedblocks.contains(right_c)) {
				visitedList.add(right_c);
				right.setLevel(point.Level() + 1);
				right.setVisited();
				right.setParent(point);
				bfsQueue.add(right);
			}

			// Left
			Point left = point.left();
			Coordinate left_c = new Coordinate(left.X(), left.Y());
			if (this.isPointValid(left) && !visitedList.contains(left_c)
					&& !this.closedblocks.contains(left_c)) {
				visitedList.add(left_c);
				left.setLevel(point.Level() + 1);
				left.setVisited();
				left.setParent(point);
				bfsQueue.add(left);
			}

			// Up
			Point up = point.up();
			Coordinate up_c = new Coordinate(up.X(), up.Y());
			if (this.isPointValid(up) && !visitedList.contains(up_c)
					&& !this.closedblocks.contains(up_c)) {
				visitedList.add(up_c);
				up.setLevel(point.Level() + 1);
				up.setVisited();
				up.setParent(point);
				bfsQueue.add(up);
			}

			// Down
			Point down = point.down();
			Coordinate down_c = new Coordinate(down.X(), down.Y());
			if (this.isPointValid(down) && !visitedList.contains(down_c)
					&& !this.closedblocks.contains(down_c)) {
				visitedList.add(down_c);
				down.setLevel(point.Level() + 1);
				down.setVisited();
				down.setParent(point);
				bfsQueue.add(down);
			}
		}

		return INFINITE;
	}

	private String pathKey(Coordinate current, List<Coordinate> cos) {

		String key = "";
		List<Integer> coordinateKeys = new ArrayList<Integer>();

		for (Coordinate co : cos) {
			coordinateKeys.add(xmax * co.Y() + co.X());
		}
		Collections.sort(coordinateKeys);

		for (Integer i : coordinateKeys) {
			key += i + ":";
		}
		int cur = (current.Y() * xmax + current.X());
		return cur + "-" + key;
	}

	private String edgeKey(Coordinate co1, Coordinate co2) {

		String key = "";

		if (co1.X() > co2.X()) {
			key = key + co1.X() + "-" + co2.X();
		} else {
			key = key + co2.X() + "-" + co1.X();
		}

		if (co1.Y() > co2.Y()) {
			key = key + co1.Y() + "-" + co2.Y();
		} else {
			key = key + co2.Y() + "-" + co1.Y();
		}

		return key;
	}

	private boolean isPointValid(Point point) {
		if (point.X() >= this.xmax || point.X() < this.xmin)
			return false;
		if (point.Y() >= this.ymax || point.Y() < this.ymin)
			return false;

		return true;
	}

	private static boolean validateInput(int width, int height,
			int numOfChekpoints) {
		if (width < 1 && width > 100)
			return false;
		if (height < 1 && height > 100)
			return false;
		if (numOfChekpoints > 18)
			return false;
		return true;
	}

}

class Point {
	private int x;
	private int y;
	private int level = 0;
	private boolean visited = false;
	private Point parent = null;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		this.visited = false;
	}

	public Point left() {
		return new Point(this.x - 1, this.y);
	}

	public Point right() {
		return new Point(this.x + 1, this.y);
	}

	public Point up() {
		return new Point(this.x, this.y + 1);
	}

	public Point down() {
		return new Point(this.x, this.y - 1);
	}

	public void setVisited() {
		this.visited = true;
	}

	public boolean isVisited() {
		return this.visited;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int Level() {
		return this.level;
	}

	public void setParent(Point parent) {
		this.parent = parent;
	}

	public Point Parent() {
		return this.parent;
	}

	public int X() {
		return this.x;
	}

	public int Y() {
		return this.y;
	}

	@Override
	public String toString() {
		return "((x,y)=(" + this.x + "," + this.y + "), level=" + this.level
				+ ")";
	}
}

class Coordinate {
	private int x;
	private int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int X() {
		return this.x;
	}

	public int Y() {
		return this.y;
	}

	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

	@Override
	public boolean equals(Object obj) {
		boolean equal = true;

		if (obj != null && obj instanceof Coordinate) {
			equal = equal && this.x == ((Coordinate) obj).x;
			equal = equal && this.y == ((Coordinate) obj).y;
			return equal;
		}
		return false;
	}
}
